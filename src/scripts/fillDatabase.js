/*
 This is file is a script that i used to populate the database.
*/
import got from 'got';
import _ from 'lodash';
import mongodb from 'mongodb';
import moment from 'moment-timezone';

/* Database params */
const connectionString = process.env.CONNECTION_STR;
let dbConnection = null;
let db = null;
const dailyWeatherCollection = 'Weather-Data';
/* Weather API params */
const dailyTempBaseUrl = process.env.CURRENT_WEATHER_URI;
const apiKey = process.env.API_KEY;

/*Supported Locations*/
const Locations = {
    rotterdam: {
        city: 'Rotterdam',
        countryCode: 'NL',
        timeZone: 'Europe/Amsterdam'
    },
    singapore: {
        city: 'Singapore',
        countryCode: 'SG',
        timeZone: 'Singapore'
    }
};

const setupDbConnection = async () => {
    const client = new mongodb.MongoClient(connectionString, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    dbConnection = await client.connect();
    db = client.db('W-Corp-Weather');
};

const tearDownConnection = async () => {
    dbConnection.close();
};

/**
 *
 * @param {string} location
 * @param {Date} date - date format YYYY-MM-DD
 * @returns data model object
 */
const getDailyWether = async (location, date) => {
    const startDate = new Date(date).toISOString().slice(0, 10);
    const endDate = new Date(date.setDate(date.getDate() + 1)).toISOString().slice(0, 10);
    const response = await got(dailyTempBaseUrl, {
        searchParams: {
            city: `${location.city},${location.countryCode}`,
            lang: 'en',
            key: apiKey,
            start_date: startDate,
            end_date: endDate
        }
    });

    // Validate response status + add error handler.
    const payload = JSON.parse(response.body);
    // simulate 24 entries
    const temp = payload.data[0].temp;
    const measurements = Array(24).fill({ temperature: temp, timestame: date });

    const res = {
        //measuredAt: new Date(payload.data[0].ts * 1000),
        startDate: moment(date).tz(location.timeZone).startOf('day').utc().toDate(),
        endDate: moment(date).tz(location.timeZone).endOf('day').utc().toDate(),
        timeZone: location.timeZone,
        countryCode: location.countryCode,
        city: location.city,
        measurements,
        mCount: 24,
        mSum: temp * 24
    };

    return res;
};

/**
 *
 */
const fetchHistoricalData = async (location) => {
    const locationObject = Locations?.[location];
    const now = new Date();
    if (!locationObject) throw 'Location does not exist';

    const dates = Array.from(
        { length: 200 },
        (val, indx) => new Date(new Date().setDate(now.getDate() - indx))
    );
    const promisesArray = _.transform(
        dates,
        (result, date) => {
            result.push(getDailyWether(locationObject, date));
        },
        []
    );
    return (await Promise.allSettled(promisesArray))
        .filter(({ status }) => status === 'fulfilled')
        .map(({ value }) => value);
};

/**
 * main function.
 */
(async () => {
    try {
        //Setup databse connection.
        await setupDbConnection();
        const dailyCollection = db.collection(dailyWeatherCollection);
        //Fetch and store historical data.
        const documentsToInsert = await fetchHistoricalData('rotterdam');
        await dailyCollection.insertMany(documentsToInsert);
        //Close connection.
        tearDownConnection();
    } catch (error) {
        /*Handle Error*/
    }
})();
